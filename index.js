console.log("Este es mi primer console log");

var message = "Este mensaje saldra en cosole.log";

console.log(message);

//String = Cadena de texto - "Hola, esto es una String"
//Number = 23
//Boolean = true/false
//null = algo esta creado, pero no tiene contenido 
//undefined = no existe, ni tiene contenido 
// Object = {nombre: "Pepe" , apellido: "Martinez"}

var name = "Javi";

var age = 25;

var isTall = false;

var haveWork = null;

var city = undefined;

console.log("Hola, me llamo", name);
console.log("Tengo", age);
console.log("¿Soy alto?", isTall );
console.log("Mi trabajo es", haveWork);
console.log("Mi ciudad es", city);


 