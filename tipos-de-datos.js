//STRING

/** 
 * set o setter significa cuando  se graba informacion, Por ejemplo
 * cuando creamos una variable estamos grabando el nombre de la variable 
 * el valor que le asignamos.
 */
var name = "Javi";


/** 
 * get o getter es cuando leemos el valor de algo, por ejemplo:
*/

console.log(name);

/** 
 * Number
*/

var age = 25;
var numberPi = 3.1416;

console.log(age);
console.log(numberPi);
console.log(age + numberPi + " Esto suma mi edad y el numero pi");
console.log(age - numberPi + " Esto resta mi edad y el numero pi");
console.log(age / numberPi + " Esto divide mi edad y el numero pi");
console.log(age * numberPi + " Esto multiplica mi edad y el numero pi");
var concatenado = 5 + 3 + " upgrade";

var concatenado2 = "upgrade" + 5 + 3;

var concatenado3 = "upgrade" + (5 + 3); 

console.log(concatenado);
console.log(concatenado2);
console.log(concatenado3);

/**
 * BOLEAN
 */

var tengoEfectivo = true;
var tengoTarjeta = false;

var puedoPagar = tengoEfectivo || tengoTarjeta; //Operador Logico OR
console.log("Puedo pagar: ", puedoPagar);

//Operador Logico AND
var tengoCoche = false;
var tengoCarnet = true;

var puedoConducir = tengoCoche && tengoCarnet;
console.log("Puedo conducir:" , puedoConducir);

//Operador Logico NOT

var soyProgramador = true;
console.log(soyProgramador);




