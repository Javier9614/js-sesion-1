var superHeroe = {
    nombre: "Peter Parker",
    edad: 14,
    esSuperheroe: true, 
};

/**
 * Formas de acceder a un objeto->
 * dot notation : notificacion de puntos, es decir, superhero.nombre
 * bracket notation: superheroe["nombre"] 
 */

console.log(superHeroe.nombre);
console.log (superHeroe.esSuperHeroe);

//Objero nombre casa, propiedades: 

var casa = {
    vasos: 4,
    platos: 5,
};

var nombreDelCampoPlatos = "platos";

console.log(" en mi casa hay: " , casa.vasos + " vasos" );
console.log("y tambien tengo: " + casa[nombreDelCampoPlatos] + " platos" );